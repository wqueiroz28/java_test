package com.fiap.gestaofesta.controller;


import com.fiap.gestaofesta.model.Convidado;
import com.fiap.gestaofesta.repository.Convidados;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ConvidadosController {

    @Autowired
    private Convidados convidados;
    private Convidado convidado;



    @GetMapping
    public ModelAndView listar(){


        ModelAndView modelAndView = new ModelAndView("ListaConvidados");

        modelAndView.addObject("convidados" , convidados.findAll());

        return modelAndView;

    }








}
